Rails.application.routes.draw do
  mount Ckeditor::Engine => '/ckeditor'
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"


  root 'questions#index'

  get 'login' => "admin#index", as: "login"
  post 'login' => "admin#create"
  get 'logout' => "admin#destroy", as: "logout"

  resources :questions, path: '', shallow: true do
    resources :answers, except: [:index, :show]
  end

  resources :instructions, except: [:new, :create]

  get ':id/:answer_id/empty' => "questions#empty", as: "empty"
  get 'new/:answer_id' => "questions#new", as: "new_fat_question"
  post 'fat/:answer_id' => "questions#create", as: 'question_answer'
  get 'new/int/:answer_id' => "instructions#new", as: "new_instruction"
  post 'int/:answer_id' => "instructions#create", as: 'instruction_answer'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
