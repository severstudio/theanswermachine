class Instruction < ActiveRecord::Base
  has_one :answer, as: :children, dependent: :destroy

  validates :title, presence: true,
            length: { minimum: 1, maximum: 150 }

  validates :text, presence: true
end
