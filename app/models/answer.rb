class Answer < ActiveRecord::Base
  belongs_to :question
  belongs_to :children, polymorphic: true, dependent: :destroy

  validates :title, presence: true,
            length: { maximum: 200 }
end
