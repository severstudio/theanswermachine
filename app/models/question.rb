class Question < ActiveRecord::Base
  has_many :answers, dependent: :destroy
  has_one :answer, as: :children
  validates :title, presence: true,
            length: { minimum: 1, maximum: 200 }

end
