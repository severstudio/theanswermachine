class AdminController < ApplicationController
  def index

  end
  def create
    @adminname = params.require(:admin).permit(:login)[:login]
    @adminpass = params.require(:admin).permit(:password)[:password]
    @admin = Admin.find_by(login: @adminname, password: @adminpass)
    if  @admin
      session[:admin] = true
      flash[:notice] = "Вы авторизировались"
      redirect_to root_path
    else
      flash[:notice] = "Неверный логин или пароль"
      redirect_to login_path
    end
  end
  def destroy
    reset_session
    flash[:notice] = "Вы вышли из системы"
    redirect_to login_path
  end
end
