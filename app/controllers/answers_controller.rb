class AnswersController < ApplicationController
  before_action :set_answer, only: [:edit, :update, :destroy]
  before_action :set_question, only: [:new, :create]
  before_action :authorize, except: [:index]
  def index
    @answer = Answer.first
  end

  def new
    @answer = @question.answers.new
  end

  def create
    @answer = @question.answers.new(answer_params)
    respond_to do |format|
      if @answer.save
        flash[:notice] = "Ответ удачно создан"
        format.html { redirect_to @answer.question}
        format.js   { }
        format.json { render @question, status: :created, location: @answer }
      else
        format.html { render :new }
        format.js   { }
        format.json { render json: @answer.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @answer.update(answer_params)
        format.html { redirect_to @answer.question, notice: 'Ответ успешно обновлен' }
        format.json { render :show, status: :ok, location: @answer }
      else
        format.html { render :edit }
        format.json { render json: @answer.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @parent = @answer.question
    @answer.destroy
    respond_to do |format|
      format.html { redirect_to @parent, notice: 'Ответ удачно удален' }
      format.json { head :no_content }
    end
  end
  private

  def set_answer
    @answer = Answer.find(params[:id])
  end
  def answer_params
    params.require(:answer).permit(:title)
  end

  def set_question
    @question = Question.find(params[:question_id])
  end


end
