class QuestionsController < ApplicationController
  before_action :set_question, only: [:show, :empty, :edit, :update, :destroy]
  before_action :set_answer, only: [:new, :create, :empty]
  before_action :authorize, except: [:index, :show, :empty]


  def index
    @question = Question.first
  end

  def show

  end

  def empty

  end

  def new
    @question = Question.new
  end

  def create
    @question = Question.new(question_params)
    respond_to do |format|
      if @question.save
        if @answer.present?
          @question.answer = @answer
        end
        flash[:notice] = "Вопрос удачно создан"
        format.html { redirect_to @question}
        format.js   { }
        format.json { render :index, status: :created, location: @question }
      else
        format.html { render :new }
        format.js   { }
        format.json { render json: @question.errors, status: :unprocessable_entity }
      end
    end
  end


  def edit
  end

  def update
    respond_to do |format|
      if @question.update(question_params)
        format.html { redirect_to @question, notice: 'Вопрос успешно обновлен' }
        format.json { render :show, status: :ok, location: @question }
      else
        format.html { render :edit }
        format.json { render json: @question.errors, status: :unprocessable_entity }
      end
    end
  end
  def destroy
    @parent = @question.answer.question
    @question.destroy
    respond_to do |format|
      format.html { redirect_to @parent, notice: 'Вопрос удачно удален' }
      format.json { head :no_content }
    end
  end
  private

    def set_answer
      @answer = Answer.find(params[:answer_id])
    end
    def set_question
      @question = Question.find(params[:id])
    end

    def question_params
      params.require(:question).permit(:title)
    end



end
