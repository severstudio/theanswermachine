class InstructionsController < ApplicationController
  before_action :set_instruction, only: [:show, :edit, :update]
  before_action :set_answer, only: [:new, :create]
  before_action :authorize, except: [:show]

  def show

  end
  def new
    @instruction = Instruction.new
  end

  def create
    @instruction = Instruction.new(instruction_params)
    respond_to do |format|
      if @instruction.save
        if @answer.present?
          @instruction.answer = @answer
        end
        flash[:notice] = "Иструкция удачно создана"
        format.html { redirect_to @instruction}
        format.js   { }
        format.json { render :show, status: :created, location: @instruction }
      else
        format.html { render :new }
        format.js   { }
        format.json { render json: @instruction.errors, status: :unprocessable_entity }
      end
    end
  end


  def edit
  end

  def update
    respond_to do |format|
      if @instruction.update(instruction_params)
        format.html { redirect_to @instruction, notice: 'Инструкция успешно обновлена' }
        format.json { render :show, status: :ok, location: @instruction }
      else
        format.html { render :edit }
        format.json { render json: @instruction.errors, status: :unprocessable_entity }
      end
    end
  end

  private

  def set_answer
    @answer = Answer.find(params[:answer_id])
  end
  def set_instruction
    @instruction = Instruction.find(params[:id])
  end

  def instruction_params
    params.require(:instruction).permit(:title, :text)
  end

end
