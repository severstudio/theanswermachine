class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  helper_method :admin?

  protected

  def admin?
    session[:admin]
  end

  def authorize
    unless admin?
      flash[:error] = "Доступ запрещен"
      redirect_to root_path
      false
    end
  end
end
